#!/usr/local/bin/python3

import openpyxl
import csv
import glob
import logging
import sys

ITEMNO = 1
PORA = 2
DESCRIPT = 3
PARTNO = 6
ASSYQTY = 10
REFDESMEMO = 4

here = 'HEREHEREHEREHEREHEREHERE'

firstItemRow = 10

PRODCHK = 1
ITEMNO = 2
MASTERNO = 3
CUSTOMERPN = 4
MFCPN = 5 
QTY = 6
REFERENCE = 7
DESCRIPTION = 8
PACKAGEFOOTPRINT = 9
MFR = 10
MFRPN = 11
COMMENTS = 12
TTLPKGS = 13
QTYRECEIVED = 14
QTYREQ = 15
QTYISSUE = 16
QTYBAL = 17
INLINESHORT = 18
QTYTORETURN = 19
KITRETURNQTY = 20
KITRETURNCOMMENTS = 21

TOTAL_ENTRIES = -1 #set by collectData

componentsC = [PRODCHK,ITEMNO,MASTERNO,CUSTOMERPN,MFCPN,QTY,REFERENCE,\
DESCRIPTION,PACKAGEFOOTPRINT,MFR,MFRPN,COMMENTS,TTLPKGS,QTYRECEIVED,QTYREQ,\
QTYISSUE,QTYBAL,INLINESHORT,QTYTORETURN,KITRETURNQTY,KITRETURNCOMMENTS]

skipCase = [QTYBAL,QTYTORETURN,QTYRECEIVED]
noCase = [ITEMNO, MASTERNO]
carryOverCase = [DESCRIPTION,MFR,MFRPN,COMMENTS]
subtotalCase = [QTY,QTYREQ]
populateCase = [QTYRECEIVED,QTYISSUE]

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='qtb.log',
                    filemode='w')


#TODO populate qtyissue whitespot
#TODO populate qtyreceived whitespot
#TODO add surplus to the right
def _toitemno(n):
    head = ''
    number = n
    if not str(n[-1]).isdigit():
        head = n[-1]
        number = n[:-1]
    if int(number) < 10:
        return '000'+ n
    elif int(number) < 100:
        return '00'+ n
    elif int(number) < 1000:
        return '0' + n
    else:
        return n

#returns (totalqty, first)
def _findDupes(l):
    seen = {}
    dupes = {}
    poraitemno = {} #for log
    logTxt = '\n\nMerged items:\n' #for log
    c = 0
    for (pora,assyqty, itemno) in l:
        if not str(assyqty)[0].isdigit():
            continue
        assyqtyInt = int(assyqty)
        if pora not in seen:
            seen[pora] = [1,assyqtyInt,itemno] # [seenNumber, totalQty, itemno]
        else:

            if seen[pora][0] == 1:

                dupes[pora] = [assyqtyInt+seen[pora][1],True] #add qty from seen

                #for log
                poraitemno[pora] = [seen[pora][2]] #itemno
                poraitemno[pora].append(itemno)
            elif seen[pora][0] > 1:

                dupes[pora][0] = dupes[pora][0] + assyqtyInt #add qty from dupes

                #for log
                poraitemno[pora].append(itemno) 
            seen[pora][0] += 1
        c += 1

    #logging
    for key, value in dupes.items():
        logTxt = logTxt + '\nPARTNO: {} - combined total qty: {}\n'.format(key,value[0])
        for itemno in poraitemno[key]:
            logTxt = logTxt + ' - {}\n'.format(itemno)
    logging.info(logTxt)

    return dupes

#get item numOfManu tuple
def getItemNumOfManuTuple(column):
    count = 1
    first = True
    item_numOfManuArr = []
    for entry in column:
        if entry.value is None:
            count += 1
            continue
        value = str(entry.value)


        #itemno encounter case
        #item item field is an integer or first digit is a digit
        if isinstance(entry.value, int) or (isinstance(entry.value, str) and entry.value[0].isdigit()):
            if first:
                firstItemi = count
                itemi = count
                itemRaw = entry
                itemValue = value
                count += 1
                first = False
                continue
            


            numOfManu = count - itemi

            #yellow color case
            colori=str(itemRaw.fill.start_color.index)

            #append only if the cell is not yellow
            if 'FFFFFF00' in colori:
                logging.info('\n\nITEMNO: {} - DNI. Skipping\n'.format(_toitemno(itemValue)))
            elif '0' in colori and len(colori) < 2:
                logging.info('\n\nITEMNO: {} - Notes for assembly, not component to quote/buy. Skipping\n'.format(_toitemno(itemValue)))
            else:
                item_numOfManuArr.append((itemValue,numOfManu,itemi))
            
            itemi = count
            itemRaw = entry
            itemValue = value
            lastManuCount = count + 1 #anticipating next count++

        count += 1

    #getting the last entry
    # numOfManu = lastManuCount - itemi #lastManuCount saved for last manu count
    numOfManu = 5 #lastManuCount saved for last manu count

    item_numOfManuArr.append((itemValue,numOfManu,itemi))

    return item_numOfManuArr
def getSourceFiles(path):
    files = glob.glob(path)
    sourcefiles = []
    for file in files:
        #mac syntax
        if len(file.split('/')) > 1:
            excelname = file.split('/')[-1]
        #window syntax
        else:
            excelname = file.split('\\')[-1]
        
        if excelname.startswith('SO#') and excelname.endswith('.xlsx'):
            sourcefiles.append(file)
    
    return sourcefiles

def getTemplateFile(path):
    files = glob.glob(path)
    for file in files:
        #mac syntax
        if len(file.split('/')) > 1:
            excelname = file.split('/')[-1]
        #window syntax
        else:
            excelname = file.split('\\')[-1]
        if 'TEMPLATE' in excelname and excelname.endswith('.xlsx'):
            return file

def getNewPartNo(value):
    if value.startswith('ALC'):
        return value # ?B
    return 'ALC' + value # ?B

def collectData(prodchk,sSheet, tSheet, ce, offset, dic): #source,target,coefficient,offset

    sCurrItemRow = firstItemRow
    tCurrItemRow = firstItemRow

    masterno = 1
    while True:
        #if itemno and custoner p/n don't exist
        
        '''
        {mfcno:
            [ 
                {PRODCHK: 'SO #12345',
                ITEMNO: '1',
                ...},{PRODCHK: 'SO #12345',
                ITEMNO: '1',
                ...}
            ],
        mfcno2:
        }
        '''
        tempList = {}
        SORT = CUSTOMERPN
        if sSheet.cell(row=sCurrItemRow, column=CUSTOMERPN).value is None:
            if sSheet.cell(row=sCurrItemRow, column=ITEMNO).value is None:
                break
            SORT = MFRPN
        componentsDic = {}
        for componentC in componentsC:
            #curr
            if componentC is PRODCHK:
                assignee = prodchk
            elif componentC is ITEMNO:
                assignee = sSheet.cell(row=sCurrItemRow, column=ITEMNO).value
            # elif componentC is QTYBAL:
            #     assignee = '=P{}-O{}'.format(tCurrItemRow+offset,tCurrItemRow+offset)
            elif componentC is QTYREQ:
                qtyInt = int(sSheet.cell(row=sCurrItemRow, column=QTY-1).value)
                qtyBuildInt = int(sSheet.cell(row=5,column=4).value) #position hardcoded to QtyBuild
                assignee = qtyInt*qtyBuildInt
            elif componentC is MASTERNO:
                assignee = masterno
            elif componentC in skipCase: #don't change [QTYBAL,QTYTORETURN,QTYRECEIVED]
                assignee = tSheet.cell(row=tCurrItemRow+offset, column=componentC).value
            elif componentC is QTYISSUE:
                assignee = 0
            # elif componentC is QTYTORETURN:
            #     pos = openpyxl.utils.get_column_letter(QTYBAL) + str(tCurrItemRow+offset)
            #     assignee = '={}'.format(pos)
            # elif componentC is QTYRECEIVED:
            #     pos = openpyxl.utils.get_column_letter(QTYISSUE) + str(tCurrItemRow+offset)
            #     assignee = '={}'.format(pos)

            elif componentC is QTY:
                assignee = sSheet.cell(row=sCurrItemRow, column=componentC-1).value
                if assignee is '':
                    assignee = sSheet.cell(row=sCurrItemRow, column=componentC-1).value #TODO
            else:
                assignee = sSheet.cell(row=sCurrItemRow, column=componentC-1).value
            componentsDic[componentC] = assignee
        sCurrItemRow += 1
        tCurrItemRow += ce+1
        sortBy = componentsDic[SORT]
        if sortBy in dic:
            dic[sortBy].append(componentsDic)
        else:
            dic[sortBy] = [componentsDic]
        masterno += 1

    global TOTAL_ENTRIES
    TOTAL_ENTRIES = masterno
    return dic
'''
{PRODCHK: 'SO #12345',
                ITEMNO: '1',
                ...}
'''
def applyTotal(dic,sheet, num):
    currItemRow = firstItemRow
    darkYellowFill = openpyxl.styles.PatternFill(start_color='FAC001',
                   end_color='FAC001',
                   fill_type='solid')
    masterno = 1
    for _,itemArr in dic.items(): #all rows of customerpn
        for smallDic in itemArr: #each row
            for key,value in smallDic.items(): #key: PRODCHK, value:'SO #12345'
                if key is MASTERNO:
                    sheet.cell(row=currItemRow, column=key).value = masterno
                elif key in skipCase:
                    continue
                else:
                    sheet.cell(row=currItemRow, column=key).value = value
            currItemRow += 1

        #total
        numOfItems = len(itemArr)
        for componentC in componentsC:
            sheet.cell(row=currItemRow, column=componentC).fill = darkYellowFill
            if componentC in noCase:
                # print(currItemRow)
                assignee = sheet.cell(row=currItemRow-numOfItems, column=MASTERNO).value
            elif componentC in skipCase:
                continue
            elif componentC is MFCPN:
                cellPos = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-numOfItems)
                assignee = '=CONCATENATE({},CHAR(10),"Total")'.format(cellPos)
            elif componentC in carryOverCase:
                assignee = sheet.cell(row=currItemRow-numOfItems, column=componentC).value
            elif componentC in subtotalCase: #=SUBTOTAL(9,On-2:On-1)
                cellPos1 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-numOfItems)
                cellPos2 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-1)
                assignee = '=SUBTOTAL(9,{}:{})'.format(cellPos1,cellPos2)
            # elif componentC is QTYRECEIVED:
            #     pos = openpyxl.utils.get_column_letter(QTYISSUE) + str(currItemRow)
            #     assignee = '={}'.format(pos)
            elif componentC is QTYISSUE:
                remainder = '-' + openpyxl.utils.get_column_letter(componentC) + str(currItemRow-numOfItems)

                for i in range(numOfItems):
                    curr = currItemRow-(numOfItems-i) #from first item
                    #P13

                    #if only one item
                    if numOfItems is 1:
                        assignee = '=' + openpyxl.utils.get_column_letter(componentC) + str(currItemRow)

                    #last case
                    elif i is numOfItems-1:  #=IF(Q11<0,0,P13-P10-P11)
                        pos1 = openpyxl.utils.get_column_letter(QTYBAL) + str(currItemRow-2)
                        pos2 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow) + remainder
                        assignee = '=IF({}<0,0,{})'.format(pos1,pos2)

                        '''
                        =IF(Q10<0,0,IF(P13-P10>O11,O11,P13-P10)) #for first
                        =IF(Q11<0,0,IF(P13-P10-P11>O12,O12,P13-P10-P11)) #for middle
                        =IF(Q11<0,0,P13-P10-P11) #for last
                        '''
                    #first case
                    elif i is 0:  #=IF((P13<O10),P13,O10)
                        pos1 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow)
                        pos2 = openpyxl.utils.get_column_letter(QTYREQ) + str(currItemRow-numOfItems)

                        assignee = '=IF(({first}<{second}),{first},{second})'.format(first=pos1,second=pos2)

                    #normal case
                    else:  #=IF(Q10<0,0,IF(P13-P10>O11,O11,P13-P10))

                        pos1 = openpyxl.utils.get_column_letter(QTYBAL) + str(curr-1)                        
                        pos2 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow) + remainder
                        pos3 = openpyxl.utils.get_column_letter(QTYREQ) + str(curr)

                        remainder = remainder + '-' + openpyxl.utils.get_column_letter(componentC) + str(curr)

                        assignee = '=IF({first}<0,0,IF({second}>{third},{third},{second}))'.format(first=pos1,second=pos2,third=pos3)
                    sheet.cell(row=curr, column=componentC).value = assignee

                assignee = 0
            else:
                assignee = ''
            sheet.cell(row=currItemRow, column=componentC).value = assignee
        # currItemRow = currItemRow + (numOfItems+1)
        currItemRow += 1
        masterno += 1
    # currItemRow = firstItemRow + num
    # lastMasterNo = TOTAL_ENTRIES
    

    # for i in range(1,lastMasterNo):
    #     for componentC in componentsC:
    #         sheet.cell(row=currItemRow, column=componentC).fill = darkYellowFill
    #         if componentC in noCase:
    #             assignee = sheet.cell(row=currItemRow-num, column=MASTERNO).value
    #         elif componentC in skipCase:
    #             assignee = sheet.cell(row=currItemRow, column=componentC).value
    #         elif componentC is MFCPN:
    #             cellPos = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-num)
    #             assignee = '=CONCATENATE({},CHAR(10),"Total")'.format(cellPos)
    #         elif componentC in carryOverCase:
    #             assignee = sheet.cell(row=currItemRow-num, column=componentC).value
    #         elif componentC in subtotalCase: #=SUBTOTAL(9,On-2:On-1)
    #             cellPos1 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-num)
    #             cellPos2 = openpyxl.utils.get_column_letter(componentC) + str(currItemRow-1)
    #             assignee = '=SUBTOTAL(9,{}:{})'.format(cellPos1,cellPos2)
    #         # elif componentC is QTYRECEIVED:
    #         #     pos = openpyxl.utils.get_column_letter(QTYISSUE) + str(currItemRow)
    #         #     assignee = '={}'.format(pos)
    #         else:
    #             assignee = ''
    #         sheet.cell(row=currItemRow, column=componentC).value = assignee
    #     currItemRow = currItemRow + (num+1)



def run():
    templatepath = 'merge/template/*.xlsx'
    sourcepath = 'merge/*.xlsx'

    templatefile = getTemplateFile(templatepath)
    sourcefiles = getSourceFiles(sourcepath)

    if templatepath is None:
        logging.error('Couldn\'t find the template file. Exiting')
        return -1        

    if not sourcefiles:
        logging.error('Couldn\'t find the source files. Exiting')
        return -1

    masterbook = openpyxl.load_workbook(templatefile)
    mastersheet = masterbook.worksheets[0]

    allItemList = []
    offset = 0
    dic = {}
    for sourcefile in sourcefiles:
        sourcebook = openpyxl.load_workbook(sourcefile)
        allSheetNames = sourcebook.sheetnames

        #iterating through sheetnames
        numOfSources = len(sourcefiles)
        
        for sheetname in allSheetNames:
            #skipping non'SO#'  and excelname.endswith('.xlsm')BOM
            if sheetname.endswith('BOM'):
                sheet = sourcebook[sheetname]
                if len(sourcefile.split('/')) is 1: #windows
                    prodchk = sourcefile.split('\\')[-1].split('(')[0]
                else: #mac
                    prodchk = sourcefile.split('/')[-1].split('(')[0]
                dic = collectData(prodchk,sheet, mastersheet, numOfSources, offset, dic)
                break

        offset += 1
    
    print(dic)

    applyTotal(dic,mastersheet,numOfSources)
    masterbook.save(sourcepath.replace('*','compiled'))


def main(argv):
    run()
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
else:
    main(sys.argv)
