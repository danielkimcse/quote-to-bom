#!/usr/local/bin/python3

import openpyxl
import csv
import glob
import logging
import sys

ITEMNO = 1
PORA = 2
DESCRIPT = 3
PARTNO = 6
ASSYQTY = 10
REFDESMEMO = 4

here = 'HEREHEREHEREHEREHEREHERE'

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='qtb.log',
                    filemode='w')
def _toitemno(n):
    head = ''
    number = n
    if not str(n[-1]).isdigit():
        head = n[-1]
        number = n[:-1]
    if int(number) < 10:
        return '000'+ n
    elif int(number) < 100:
        return '00'+ n
    elif int(number) < 1000:
        return '0' + n
    else:
        return n

#returns (totalqty, first)
def _findDupes(l):
    seen = {}
    dupes = {}
    poraitemno = {} #for log
    logTxt = '\n\nMerged items:\n' #for log
    c = 0
    for (pora,assyqty, itemno) in l:
        if not str(assyqty)[0].isdigit():
            continue
        assyqtyInt = int(assyqty)
        if pora not in seen:
            seen[pora] = [1,assyqtyInt,itemno] # [seenNumber, totalQty, itemno]
        else:

            if seen[pora][0] == 1:

                dupes[pora] = [assyqtyInt+seen[pora][1],True] #add qty from seen

                #for log
                poraitemno[pora] = [seen[pora][2]] #itemno
                poraitemno[pora].append(itemno)
            elif seen[pora][0] > 1:

                dupes[pora][0] = dupes[pora][0] + assyqtyInt #add qty from dupes

                #for log
                poraitemno[pora].append(itemno) 
            seen[pora][0] += 1
        c += 1

    #logging
    for key, value in dupes.items():
        logTxt = logTxt + '\nPARTNO: {} - combined total qty: {}\n'.format(key,value[0])
        for itemno in poraitemno[key]:
            logTxt = logTxt + ' - {}\n'.format(itemno)
    logging.info(logTxt)

    return dupes

def _getManuErrMsg(l,itemno):
    msg = '\n\nITEMNO: {} - Too many manufacturers. Skipping\n'.format(itemno)
    for manu in l:
        msg = '{} - {}\n'.format(msg,manu) 

    return msg
#get item numOfManu tuple
def getItemNumOfManuTuple(itemColumn, customerColumn):
    count = 1
    first = True
    item_numOfManuArr = []
    # itemcolumn = columns[0]
    # customercolumn = columns[1]
    stop = False
    for i in range(len(itemColumn)):
        entry = itemColumn[i]
        customer = customerColumn[i]
        if entry.value is None:
            if customer.value is None:
                count += 1
                continue

            #erroneous no itemno
            stop = True

        #stop case
        #item item field is an integer or first digit is a digit
        if (not first and stop) or isinstance(entry.value, int) or (isinstance(entry.value, str) and entry.value[0].isdigit()):
            if stop:
                stop = False

            if first:
                firstItemi = count
                itemi = count
                itemRaw = entry
                itemValue = str(entry.value)
                count += 1
                first = False
                continue
            
            numOfManu = count - itemi


            #yellow color case
            colori=str(itemRaw.fill.start_color.index)

            #append only if the cell is not yellow

            if 'FFFFFF00' in colori:
                logging.info('\n\nITEMNO: {} - DNI. Skipping\n'.format(itemValue))
            elif '0' in colori and len(colori) < 2:
                logging.info('\n\nITEMNO: {} - Notes for assembly, not component to quote/buy. Skipping\n'.format(itemValue))
            else:
                item_numOfManuArr.append((itemValue,numOfManu,itemi))
            
            itemi = count
            itemRaw = entry
            itemValue = str(entry.value)
            lastManuCount = count + 1 #anticipating next count++

        count += 1

    #getting the last entry
    # numOfManu = lastManuCount - itemi #lastManuCount saved for last manu count
    numOfManu = 5 #lastManuCount saved for last manu count

    item_numOfManuArr.append((itemValue,numOfManu,itemi))

    return item_numOfManuArr
def getSourceFile(path):
    sourcefiles = glob.glob(path)
    for file in sourcefiles:
        #mac syntax
        if len(file.split('/')) > 1:
            excelname = file.split('/')[-1]
        #window syntax
        else:
            excelname = file.split('\\')[-1]
        
        if excelname.startswith('KTL'):
            sourcefile = file
            break
    
    return sourcefile

def getNewPartNo(value):
    if value.startswith('ALC'):
        return value # ?B
    return 'ALC' + value # ?B

def run():
    with open('BOMs/BOM FORMAT TEMPLATE.csv', 'r') as f:
      reader = csv.reader(f)
      bomCsv = list(reader)

    totalCsvRow = len(bomCsv[0])

    targetHeaderIndex = [ITEMNO, PORA, DESCRIPT, PARTNO, ASSYQTY, REFDESMEMO]
    manuIndex = [19,20,21,45,46,47,48,49,50]
    modelIndex = [22,23,24,51,52,53,54,55,56]
    path = 'quotes/*.xlsx'

    sourcefile = getSourceFile(path)
    if sourcefile is None:
        logging.error('Couldn\'t find the source file. Exiting')
        return -1

    book = openpyxl.load_workbook(sourcefile, data_only=True)
    allSheetNames = book.sheetnames

    #iterating through sheetnames
    for sheetname in allSheetNames:

        #skipping non MATL COST BOM
        if not sheetname.startswith('MATL COST BOM'):
            continue
        sheet = book[sheetname]
        itemColumn = list(sheet.columns)[0]
        customerColumn = list(sheet.columns)[1]
        IQtupleArr = getItemNumOfManuTuple(itemColumn,customerColumn)

        itemnoInt = 1 #itemno counter deprecated
        for (item, numOfManu, i) in IQtupleArr:

            #no itemno
            if not sheet.cell(row=i,column=2).value:
                logging.warning('\n\nITEMNO: None - missing CUSTOMER P/N. Skipping\nCell value: {}\n'.format(chr(64+2) +str(i)))
                continue

            if not str(item[0]).isdigit():
                logging.warning('\n\nITEMNO missing. Skipping\nCell value: {}\n'.format(chr(64+1) +str(i)))
                continue
            
            itemno = _toitemno(str(item))

            #no qty
            if not sheet.cell(row=i,column=4).value:
                logging.warning('\n\nITEMNO: {} - missing QTY. Skipping\n'.format(itemno))
                continue

            #no descript and refdesmemo
            if not sheet.cell(row=i,column=5).value and not sheet.cell(row=i,column=6).value:
                logging.warning('\n\nITEMNO: {} - missing description and refdesmemo. Skipping\n'.format(itemno))
                continue

            if not sheet.cell(row=i,column=6).value:
                logging.warning('\n\nITEMNO: {} - missing description.\n'.format(itemno))
                

            if not sheet.cell(row=i,column=5).value:
                logging.warning('\n\nITEMNO: {} - missing refdesmemo.\n'.format(itemno))
                

            pora = getNewPartNo(sheet.cell(row=i,column=2).value)
            partno = pora
            
            descript = sheet.cell(row=i,column=6).value
            if descript:
                descript = sheet.cell(row=i,column=6).value.replace('\n', ' | ') # ?F
            assyqty = sheet.cell(row=i,column=4).value # ?D
            refdesmemo = sheet.cell(row=i,column=5).value # ?E

            #skip 0 qty
            if assyqty is '0':
                logging.info('\n\n ITEMNO: {} - 0 QTY. Skipping\n'.format(itemno))
                continue

            allcomp = (itemno, pora, descript, partno, assyqty, refdesmemo)
            csvRow = [''] * totalCsvRow    #array of totalCsvRow number of empty strings
            count = 0

            #setting targetHeader to csvRow
            for j in targetHeaderIndex:
                csvRow[j] = allcomp[count] if allcomp[count] else ''
                count += 1

            #manufacture and model
            
            excessManu = []
            for c in range(numOfManu):                
                
                #retrieving manu and model no
                manu = sheet.cell(row=i+c, column=7).value
                modelno = sheet.cell(row=i+c, column=8).value
                if c > 8:
                    excessManu.append(manu)
                    continue
                #end case
                if modelno and 'Total' in modelno:
                    break

                #setting manu and modelno
                csvRow[manuIndex[c]] = manu
                csvRow[modelIndex[c]] = modelno

            bomCsv.append(csvRow) 
            itemnoInt += 1

            if len(excessManu) > 0:
                logging.info(_getManuErrMsg(excessManu,itemno))
        #merge same pora
        poraqtyArr = []
        for i in range(len(bomCsv)):
            poraqtyArr.append((bomCsv[i][PORA],bomCsv[i][ASSYQTY],bomCsv[i][ITEMNO]))
        dupes = _findDupes(poraqtyArr) #(pora,assqty,itemno)
        cleari = []
        for i in range(len(bomCsv)):
            #if pora in dupe
            if bomCsv[i][PORA] in dupes:
                #if first
                if dupes[bomCsv[i][PORA]][1]:
                    bomCsv[i][ASSYQTY] = dupes[bomCsv[i][PORA]][0]
                    dupes[bomCsv[i][PORA]][1] = False

                #add to cleari
                else:
                    cleari.append(i)
        #delete dup items
        dupitems = {}
        for i in sorted(cleari, reverse=True):
            del bomCsv[i]

        #deprecated
        #re-number itemno
        # itemnoInt = 1
        # for i in range(1,len(bomCsv)):
        #     bomCsv[i][ITEMNO] = str(itemnoInt))
        #     itemnoInt += 1


    outputfile = sourcefile.split('.')[0] + '_formatted.csv'
    with open(outputfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(bomCsv)

def main(argv):
    # args = parse_args(argv)
    # logger.info('Watching %s for changes, destination is %s',
    #             args.src_folder, args.dest_folder)
    # run(args.src_folder, args.dest_folder)
    run()
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
else:
    main(sys.argv)
# main(sys.argv)