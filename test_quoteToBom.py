#!/usr/local/bin/python3 -m unittest

import unittest
import quoteToBom

class TestQuoteToBom(unittest.TestCase):

    def test__toitemnoHP(self):
       result = quoteToBom._toitemno('5')
       self.assertEqual(result, '0005')

    def test__toitemno2digits(self):
       result = quoteToBom._toitemno('34')
       self.assertEqual(result, '0034')

    def test__toitemno3digits(self):
       result = quoteToBom._toitemno('434')
       self.assertEqual(result, '0434')

    def test__toitemno4digits(self):
       result = quoteToBom._toitemno('3134')
       self.assertEqual(result, '3134')

    def test__toitemnoWithChar(self):
       result = quoteToBom._toitemno('183F')
       self.assertEqual(result, '0183F')

    def test__findDupesHP(self):
        poraqtyArr = [
            ('ALC50-1216-01', '1', '0190C'),
            ('ALC50-1215-01', '1', '0190D'),
            ('ALC80-1641-00', '1', '0191'),
            ('ALC80-1641-01', '1', '0191A'),
            ('ALC32-0388-02', '1', '0191B'),
            ('ALC50-1216-01', '1', '0191C')
            ]
        result = quoteToBom._findDupes(poraqtyArr)
        self.assertEqual(result, {'ALC50-1216-01': [2, True]})

    def test__findDupesNone(self):
        poraqtyArr = [
            ('ALC50-1216-01', '1', '0190C'),
            ('ALC50-1215-01', '1', '0190D'),
            ('ALC80-1641-00', '1', '0191'),
            ('ALC80-1641-01', '1', '0191A'),
            ('ALC32-0388-02', '1', '0191B')
            ]
        result = quoteToBom._findDupes(poraqtyArr)
        self.assertEqual(result, {})

